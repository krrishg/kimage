package com.krrishg;

import org.apache.commons.io.FileUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.Objects;
import java.util.Optional;

public class ImageUtils {

    public static String encodeImageToBase64(File file) throws IOException {
        byte[] fileContent = FileUtils.readFileToByteArray(file);
        return Base64.getEncoder().encodeToString(fileContent);
    }

    public static String encodeImageToBase64(BufferedImage image, String format) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, format, outputStream);
        outputStream.flush();

        byte[] imageBytes = outputStream.toByteArray();
        outputStream.close();

        return Base64.getEncoder().encodeToString(imageBytes);
    }

    public static String combineTwoImagesVertically(String firstImageBase64, String secondImageBase64, String format)
            throws IOException {
        if (firstImageBase64 == null) {
            firstImageBase64 = "";
        }

        if (secondImageBase64 == null) {
            secondImageBase64 = "";
        }

        BufferedImage firstImage = null, secondImage = null;

        if (!Objects.equals(firstImageBase64, "")) {
            firstImage = getImageFromBase64(firstImageBase64);
        }

        if (!Objects.equals(secondImageBase64, "")) {
            secondImage = getImageFromBase64(secondImageBase64);
        }

        int firstImageWidth = Optional.ofNullable(firstImage).map(BufferedImage::getWidth).orElse(1);
        int secondImageWidth = Optional.ofNullable(secondImage).map(BufferedImage::getWidth).orElse(1);
        int firstImageHeight = Optional.ofNullable(firstImage).map(BufferedImage::getHeight).orElse(1);
        int secondImageHeight = Optional.ofNullable(secondImage).map(BufferedImage::getHeight).orElse(1);

        int combinedWidth = Math.max(firstImageWidth, secondImageWidth);
        int combinedHeight = firstImageHeight + secondImageHeight;

        BufferedImage combinedImage = new BufferedImage(combinedWidth, combinedHeight, BufferedImage.TYPE_3BYTE_BGR);

        if (firstImage != null) {
            combinedImage.getGraphics().drawImage(firstImage, 0, 0, null);
        }

        if (secondImage != null) {
            combinedImage.getGraphics().drawImage(secondImage, 0, firstImageHeight + 10, null);
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(combinedImage, format, outputStream);
        byte[] imageBytes = outputStream.toByteArray();
        return Base64.getEncoder().encodeToString(imageBytes);
    }

    public static BufferedImage getImageFromBase64(String base64) throws IOException {
        byte[] bytes = Base64.getDecoder().decode(base64);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        return ImageIO.read(inputStream);
    }

    public static String combineTwoImagesHorizontally(String firstImageBase64, String secondImageBase64, String format)
            throws IOException {
        if (firstImageBase64 == null) {
            firstImageBase64 = "";
        }

        if (secondImageBase64 == null) {
            secondImageBase64 = "";
        }

        BufferedImage firstImage = null, secondImage = null;

        if (!Objects.equals(firstImageBase64, "")) {
            firstImage = getImageFromBase64(firstImageBase64);
        }

        if (!Objects.equals(secondImageBase64, "")) {
            secondImage = getImageFromBase64(secondImageBase64);
        }

        int firstImageWidth = Optional.ofNullable(firstImage).map(BufferedImage::getWidth).orElse(1);
        int secondImageWidth = Optional.ofNullable(secondImage).map(BufferedImage::getWidth).orElse(1);
        int firstImageHeight = Optional.ofNullable(firstImage).map(BufferedImage::getHeight).orElse(1);
        int secondImageHeight = Optional.ofNullable(secondImage).map(BufferedImage::getHeight).orElse(1);

        int combinedWidth = firstImageWidth + secondImageWidth;
        int combinedHeight = Math.max(firstImageHeight, secondImageHeight);

        BufferedImage combinedImage = new BufferedImage(combinedWidth, combinedHeight, BufferedImage.TYPE_3BYTE_BGR);

        if (firstImage != null) {
            combinedImage.getGraphics().drawImage(firstImage, 0, 0, null);
        }

        if (secondImage != null) {
            combinedImage.getGraphics().drawImage(secondImage, firstImageWidth + 10, 0, null);
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(combinedImage, format, outputStream);
        byte[] imageBytes = outputStream.toByteArray();
        return Base64.getEncoder().encodeToString(imageBytes);
    }

    public static BufferedImage cropImage(String image, int startX, int startY, int width, int height) throws IOException {
        BufferedImage originalImage = getImageFromBase64(image);

        width = Math.min(width, originalImage.getWidth() - startX);
        height = Math.min(height, originalImage.getHeight() - startY);

        return originalImage.getSubimage(startX, startY, width, height);
    }

    public static BufferedImage getBufferedImage(String imageBase64) throws IOException {
        byte[] imageContent = Base64.getDecoder().decode(imageBase64);
        return ImageIO.read(new ByteArrayInputStream(imageContent));
    }

    public static BufferedImage adjustContrast(BufferedImage image, double contrastFactor) {
        BufferedImage contrastedImage = new BufferedImage(image.getWidth(), image.getHeight(),
                BufferedImage.TYPE_INT_RGB);

        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                int rgb = image.getRGB(x, y);
                int alpha = (rgb >> 24) & 0xFF;
                int red = (rgb >> 16) & 0xFF;
                int green = (rgb >> 8) & 0xFF;
                int blue = rgb & 0xFF;

                red = (int) clamp(contrastFactor * (red - 128) + 128);
                green = (int) clamp(contrastFactor * (green - 128) + 128);
                blue = (int) clamp(contrastFactor * (blue - 128) + 128);

                int newRGB = (alpha << 24) | (red << 16) | (green << 8) | blue;
                contrastedImage.setRGB(x, y, newRGB);
            }
        }
        return contrastedImage;
    }

    private static double clamp(double value) {
        return Math.min(Math.max(value, 0), 255);
    }

    public static BufferedImage convertToBlackAndWhite(BufferedImage image) {
        return changeColorScheme(image, BufferedImage.TYPE_BYTE_BINARY);
    }

    private static BufferedImage changeColorScheme(BufferedImage image, int typeByteGray) {
        BufferedImage blackAndWhiteImage = new BufferedImage(image.getWidth(), image.getHeight(),
                typeByteGray);
        blackAndWhiteImage.getGraphics().drawImage(image, 0, 0, null);
        return blackAndWhiteImage;
    }

    public static BufferedImage convertToGrayScale(BufferedImage image) {
        return changeColorScheme(image, BufferedImage.TYPE_BYTE_GRAY);
    }

    public static BufferedImage rotateImage(BufferedImage image, int degrees) {
        int width = image.getWidth();
        int height = image.getHeight();

        int newWidth = (degrees == 90 || degrees == -90) ? height : width;
        int newHeight = (degrees == 90 || degrees == -90) ? width : height;

        BufferedImage rotatedImage = new BufferedImage(newWidth, newHeight, image.getType());
        Graphics2D g2d = rotatedImage.createGraphics();

        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        AffineTransform at = new AffineTransform();
        at.rotate(Math.toRadians(degrees), newWidth / 2.0, newHeight / 2.0);
        g2d.transform(at);
        g2d.drawImage(image, (newWidth - width) / 2, (newHeight - height) / 2, null);

        g2d.dispose();

        return rotatedImage;
    }

}
